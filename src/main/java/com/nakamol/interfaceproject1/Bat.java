/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.interfaceproject1;

/**
 *
 * @author OS
 */
public class Bat extends Poultry {

    private String nickname;

    public Bat(String nickname) {
        super("Bat", 2);
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println("Bat name is : " + nickname + " eat at night ");
    }

    @Override
    public void speak() {
        System.out.println("Bat name is : " + nickname + " speak at night ");
    }

    @Override
    public void sleep() {
        System.out.println("Bat name is : " + nickname + " sleep at night ");
    }

    @Override
    public void fly() {
        System.out.println("Bat name is : " + nickname + " fly at night ");
    }

}
