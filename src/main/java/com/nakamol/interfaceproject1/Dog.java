/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.interfaceproject1;

/**
 *
 * @author OS
 */
public class Dog extends LandAnimal {

    private String nickname;

    public Dog(String nickname) {
        super("Dog", 4);
        this.nickname = nickname;
    }

    /**
     *
     */
    @Override
    public void eat() {
        System.out.println("Dog name is : " + nickname + " Can eat");
    }

    @Override
    public void speak() {
        System.out.println("Dog name is : " + nickname + " speak >> box box !!!");
    }

    @Override
    public void sleep() {
        System.out.println("Dog name is : " + nickname + " Can sleep");
    }

    @Override
    public void run() {
        System.out.println("Dog: run");
        System.out.println("Dog name is : " + nickname + " Can run");
    }
}
