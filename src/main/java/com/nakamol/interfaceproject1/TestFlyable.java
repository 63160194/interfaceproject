/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.interfaceproject1;

/**
 *
 * @author OS
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat("Batty");
        bat.fly();          //Animal, Poultry, Flyable
        
        Plane plane = new Plane("Engine number 1");
        plane.fly();      //Vahicle, Flyable
        
        Dog dog = new Dog("Puppy");
        dog.run();
        
        Car car = new Car("Engine number 2");
        car.run();

        Flyable[] flyables = {bat, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }

        Runable[] runables = {dog, plane, car};
        for (Runable r : runables) {
            if(r instanceof Car){
                Car c = (Car) r;
                c.startEngine();
                c.run();
            }
            r.run();
        }
    }
}
